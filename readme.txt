Despertando Sanación is a personal project, in which I offer my therapies and products.
Therapies like Thetahealing help us heal emotionally and bring out the best version of ourselves.
==================================================================================================

This website is based on the design of KREO

==================================================================================================

LICENSE:

KREO is released under the Creative Commons Attribution 3.0 License
(http://creativecommons.org/licenses/by/3.0/).

------------------------------------------------------------------------------------------------------ 


SUPPORT:
    
Since KREO is distributed for free, support is not offered. KREO is coded according 
to current web standards and we did our best to make the template easy to use and modify.
If you have minimum web development experience, you can easily modify the template. 
However, If you're still new to HTML and CSS, I suggest that you visit the 
following tutorials:

 - http://tutsplus.com/course/30-days-to-learn-html-and-css/
 - http://learn.shayhowe.com/html-css/

These will teach you the essentials of HTML and CSS. In addition, if you want to include
jQuery in your skill-set, you can also check out these tutorials: 

 - http://code.tutsplus.com/courses/30-days-to-learn-jquery
 - http://try.jquery.com/


------------------------------------------------------------------------------------------------------ 


GET THE LATEST VERSION:

We update our templates on a regular basis so to make sure that you have the latest version, 
always download the template files directly on our website(http://www.styleshout.com/)



-------------------------------------------------------------------------------------------------------


SOURCES AND CREDITS:

I've used the following resources as listed.

Fonts:
 - Raleway Font (https://www.google.com/fonts/specimen/Raleway)
 - Merriweather Font (http://www.google.com/fonts/specimen/Merriweather)

Icons:
 - Font Awesome (http://fortawesome.github.io/Font-Awesome/)

Stock Photos and Graphics:
 - UnSplash.com (http://unsplash.com/)
 - Morguefile.com (http://www.morguefile.com/)
 - splitshire.com (http://splitshire.com/)
 - gratisography.com (http://www.gratisography.com/)
 
Javascript Files:

 - JQuery (http://jquery.com/)
 - Modernizr (http://modernizr.com/)
 - Flexslider (http://www.woothemes.com/flexslider/)
 - jQuery Placeholder (https://github.com/mathiasbynens/jquery-placeholder)
 - jQuery Validator (http://jqueryvalidation.org/)
 - Waypoints (http://imakewebthings.com/jquery-waypoints/)
 - Magnific Popup (http://dimsemenov.com/plugins/magnific-popup/)
 - Fittext (http://fittextjs.com/)

--------------------------------------------------------------------------------------------------------- 


  

